package com.pizza365.pizza365.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.pizza365.pizza365.model.CVoucher;
import com.pizza365.pizza365.repository.IVoucherRepository;

@Service
public class VoucherService {
    @Autowired
    private IVoucherRepository voucherRepository;

    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> pVouchers = new ArrayList<CVoucher>();

            voucherRepository.findAll().forEach(pVouchers::add);

            return new ResponseEntity<>(pVouchers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
