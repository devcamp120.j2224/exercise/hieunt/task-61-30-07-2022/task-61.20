package com.pizza365.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.pizza365.model.COrder;

public interface IOrderRepository extends JpaRepository<COrder, Long> {

}
