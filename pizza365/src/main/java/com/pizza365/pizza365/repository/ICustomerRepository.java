package com.pizza365.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pizza365.pizza365.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    CCustomer findById(long id);
}