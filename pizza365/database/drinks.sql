-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 01, 2022 at 05:38 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pizza365_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` bigint(20) NOT NULL,
  `don_gia` bigint(20) DEFAULT NULL,
  `ghi_chu` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ma_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ngay_cap_nhat` bigint(20) DEFAULT NULL,
  `ngay_tao` bigint(20) DEFAULT NULL,
  `ten_nuoc_uong` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `don_gia`, `ghi_chu`, `ma_nuoc_uong`, `ngay_cap_nhat`, `ngay_tao`, `ten_nuoc_uong`) VALUES
(1, 10000, NULL, 'TRATAC', 1615177934000, 1615177934000, 'Trà tắc'),
(2, 15000, NULL, 'COCA', 1615177934000, 1615177934000, 'Cocacola'),
(3, 15000, NULL, 'PEPSI', 1615177934000, 1615177934000, 'Pepsi'),
(4, 5000, NULL, 'LAVIE', 1615177934000, 1615177934000, 'Lavie'),
(5, 40000, NULL, 'TRASUA', 1615177934000, 1615177934000, 'Trà sữa trân châu'),
(6, 15000, NULL, 'FANTA', 1615177934000, 1615177934000, 'Fanta');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
